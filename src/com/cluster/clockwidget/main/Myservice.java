package com.cluster.clockwidget.main;

import java.util.Calendar;
import java.util.Date;


import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;

public class Myservice extends Service {
	private static Integer[] numImage = new Integer[]{ R.drawable.bk0, R.drawable.bk1, R.drawable.bk2, R.drawable.bk3, 
		R.drawable.bk4, R.drawable.bk5, R.drawable.bk6, R.drawable.bk7, R.drawable.bk8, R.drawable.bk9, };
	private static Integer[] transparent = new Integer[]{ R.drawable.bk3};
		
	@Override
	public void onStart(Intent intent, int startId) {
		RemoteViews remoteViews = new RemoteViews(getPackageName(),R.layout.widget_main);
		Log.d("Myservice",new Date().toLocaleString());
		Calendar calendar = Calendar.getInstance();
		Integer yearInt = calendar.get(Calendar.YEAR);
		String year = new String(yearInt + getString(R.string.year));
		Integer dateMonth = calendar.get(Calendar.MONTH) + 1;
		Integer dateDay = calendar.get(Calendar.DATE);
		String date = new String(dateMonth + getString(R.string.month) + " "+ dateDay + getString(R.string.date));
		Integer week = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		Integer minute = calendar.get(Calendar.MINUTE);
		Integer minUD = minute % 10;
		Integer minTD = minute / 10;
		Integer hour = calendar.get(Calendar.HOUR);
		Integer hrUD = hour % 10;
		Integer hrTD = hour / 10;
		remoteViews.setTextViewText(R.id.year, year);
		remoteViews.setTextViewText(R.id.day, date);
		if (numImage[hrTD] != 1) {
			remoteViews.setImageViewResource(R.id.hrTenthsDigit, numImage[hrTD]);			
		}
		if (numImage[hrTD] != 0) {
			remoteViews.setImageViewResource(R.id.hrTenthsDigit, numImage[hrTD]);
		}
		remoteViews.setImageViewResource(R.id.hrUnitDigit, numImage[hrUD]);
				
		remoteViews.setImageViewResource(R.id.minTenthsDigit, numImage[minTD]);
		remoteViews.setImageViewResource(R.id.minUnitDigit, numImage[minUD]);
		
		
		ComponentName thisWidget = new ComponentName(this, ClockWidget.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(this);
		manager.updateAppWidget(thisWidget, remoteViews);
	}

	


	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
}
